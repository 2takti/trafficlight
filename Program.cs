﻿using System;

namespace trafficlight
{
    class Program
    {
        static void Main(string[] args)
        {
            string color;
            Console.WriteLine("What color is traffic light?"); //esimene
            color = Console.ReadLine();

            if (color == "green" || color == "roheline")
            {
                Console.WriteLine("then pedal to the metal");
            }
            else if (color == "yellow" || color == "kollane")
            {
                Console.WriteLine("hold your horses");
            }
            else if ( color == "red" || color == "punane")
            {
                Console.WriteLine("stomp the brakes");
            }
            else
            {
                Console.WriteLine("you blind maaan");
            }
        }
    }
}
